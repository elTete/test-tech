
cd ~/app

if [ ! -d ./vendor ]
then
    composer install
fi

php artisan serve --host 0.0.0.0
