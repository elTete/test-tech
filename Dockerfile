ARG COMPOSER_VERSION
ARG PHP_VERSION
ARG LINUX_DISTRIBUTION

FROM composer:${COMPOSER_VERSION} as composer

FROM php:${PHP_VERSION}-${LINUX_DISTRIBUTION} AS base

COPY --from=composer --chown=www-data:www-data /usr/bin/composer /usr/bin/composer

WORKDIR /home/www-data

FROM base AS dev

ARG USER_ID
ARG GROUP_ID

# mapping host user to container user
# see https://jtreminio.com/blog/running-docker-containers-as-current-host-user/
RUN if [ ${USER_ID:-0} -ne 0 ] && [ ${GROUP_ID:-0} -ne 0 ]; then \
    deluser www-data &&\
    if getent group www-data ; then delgroup www-data; fi &&\
    addgroup -g ${GROUP_ID} www-data &&\
    adduser -G www-data -u ${USER_ID} -D www-data &&\
    install -d -m 0755 -o www-data -g www-data /home/www-data &&\
    chown -c -f -R \
        ${USER_ID}:${GROUP_ID} \
        /home/www-data \
        # if using php-fpm
        # /var/run/php-fpm \
        # /var/lib/php/sessions \
;fi

COPY --chown=${USER_ID}:${GROUP_ID} "./launch.sh" /home/www-data

RUN chmod +x /home/www-data/launch.sh

USER www-data

CMD [ "sh", "/home/www-data/launch.sh" ]
